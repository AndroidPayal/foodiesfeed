package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class ManageAddressActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout manage_address_addAddressButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_address);

        manage_address_addAddressButton=findViewById(R.id.manage_address_addAddressButton);
        manage_address_addAddressButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.manage_address_addAddressButton:
                Intent intent = new Intent(ManageAddressActivity.this,AddAddressActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(intent);
                break;
        }
    }
}
