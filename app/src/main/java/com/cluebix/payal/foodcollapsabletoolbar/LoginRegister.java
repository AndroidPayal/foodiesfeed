package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cluebix.payal.foodcollapsabletoolbar.fragments.Login_mid;
import com.cluebix.payal.foodcollapsabletoolbar.fragments.Registration_mid;

public class LoginRegister extends AppCompatActivity implements View.OnClickListener {

    TextView loginText, registerText, submit, forgot_pw;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    FragmentTransaction transaction;

    public int RC_SIGN_IN = 200;
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        context = this;

        loginText = (TextView) findViewById(R.id.login2_logintext);
        registerText = (TextView) findViewById(R.id.login2_registertext);
        submit = (TextView) findViewById(R.id.login2_submit);
        forgot_pw = findViewById(R.id.forgotPw);
        setFragment(0);
        loginText.setTextColor(getResources().getColor(R.color.text_darkgrey));

        loginText.setOnClickListener(this);
        registerText.setOnClickListener(this);


    }
/*
    private void send_data_to_server(final String f_name, final String l_name, final String email, final String providerName) {
        //Oauth url implementation
        Log.d("TAG_oauth", "send_data_to_server: name"+f_name+" "+l_name+" mail="+email);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, oAuth_url, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                Log.d("Tag_response","server response="+response);
                try {
                    JSONObject obj=new JSONObject(response);
                    if(obj.has("error")){
                        Toast.makeText(LoginRegister.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        String success = obj.getString("success");
                        String message = obj.getString("message");
                        JSONObject userdata = obj.getJSONObject("user_data");
                        String id = userdata.getString("id");

                        if (providerName.equalsIgnoreCase("google")) {
                            Log.d("Tag_sesion", "google login done");
                            sessionManager.createLoginSession(id, f_name, l_name, email,"1");
                        }else{
                            Log.d("Tag_sesion", "fb login done");
                            sessionManager.createLoginSession(id, f_name, l_name, email,"2");

                        }
                        Log.d("Tag_sesion", "onResponse: user id="+id);

                        //                 Toast.makeText(LoginRegister.this, message, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Tag_error","volley error response="+error+"");
                Toast.makeText(LoginRegister.this, "Server Connection Failed!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> parameters= new HashMap<String, String>();
                parameters.put("f_name",f_name);
                parameters.put("l_name",l_name);
                parameters.put("email",email);
                parameters.put("provider",providerName);

                return parameters;
            }
        };

        RquestHandler.getInstance(LoginRegister.this).addToRequestQueue(stringRequest);

    }

 */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login2_logintext:
                loginText.setTextColor(getResources().getColor(R.color.black));
                registerText.setTextColor(getResources().getColor(R.color.text_lightgrey));
                setFragment(0);
                break;
            case R.id.login2_registertext:
                loginText.setTextColor(getResources().getColor(R.color.text_lightgrey));
                registerText.setTextColor(getResources().getColor(R.color.black));
                setFragment(1);
                break;


        }
    }

    private void setFragment(int fragmentNo) {

        fragmentManager = this.getSupportFragmentManager();
        switch (fragmentNo) {
            case 0:
                fragment = new Login_mid();
                submit.setText(getResources().getString(R.string.signin));
                forgot_pw.setVisibility(View.VISIBLE);
                break;
            case 1:
                fragment = new Registration_mid();
                submit.setText(getResources().getString(R.string.register));
                forgot_pw.setVisibility(View.GONE);
                break;

        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.login2_fragmentContainer, fragment).commit();
    }



/*

    private void signIn()
    {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void updateUI(GoogleSignInAccount account)
    {
        if (account==null){
         //   Toast.makeText(this, "Please login correctly", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Loggedin Successfully", Toast.LENGTH_SHORT).show();
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Log.d("TAG_name","name="+personName);

       //     LastOperation();
            */

    /**** send google data to oAuth***//*

            send_data_to_server(personGivenName,personFamilyName,personEmail,"google");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        Log.d("Tag_result","rc_sign_in="+RC_SIGN_IN+" request code="+requestCode);
        if (requestCode == RC_SIGN_IN) {
            RC_SIGN_IN=0;
            // The Task returned from this call is always completed, no need to attach  a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask)
    {
        try {
            Log.d("TAG_result", "handleSignInResult: task completed");
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Tag__", "signInResult:failed code=" + e.getStatusCode());
           String status= GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode());
            Log.d("Tag_string", "signInResult:failed code= "+status);

            */
    /*  error here we are getting exception with status code 12500 (unrecoverable error)*//*

            updateUI(null);
        }
}

*/


}