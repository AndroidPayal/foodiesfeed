package com.cluebix.payal.foodcollapsabletoolbar.models;

import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cluebix.payal.foodcollapsabletoolbar.R;

public class CustomSnackbar extends BaseTransientBottomBar<CustomSnackbar> {
    /**
     * Constructor for the transient bottom bar.
     *
     * @param parent              The parent for this transient bottom bar.
     * @param content             The content view for this transient bottom bar.
     * @param contentViewCallback The content view callback for this transient bottom bar.
     */
    protected CustomSnackbar(@NonNull ViewGroup parent, @NonNull View content, @NonNull android.support.design.snackbar.ContentViewCallback contentViewCallback) {
        super(parent, content, contentViewCallback);
    }


    private static class ContentViewCallback
            implements BaseTransientBottomBar.ContentViewCallback {

        private View content;

        public ContentViewCallback(View content) {
            this.content = content;
        }
        @Override
        public void animateContentIn(int delay, int duration) {
// add custom *in animations for your views
            // e.g. original snackbar uses alpha animation, from 0 to 1
            ViewCompat.setScaleY(content, 0f);
            ViewCompat.animate(content)
                    .scaleY(1f).setDuration(duration)
                    .setStartDelay(delay);
        }
        @Override
        public void animateContentOut(int delay, int duration) {
            // add custom *out animations for your views
            // e.g. original snackbar uses alpha animation, from 1 to 0
            ViewCompat.setScaleY(content, 1f);
            ViewCompat.animate(content)
                    .scaleY(0f)
                    .setDuration(duration)
                    .setStartDelay(delay);
        }
    }




    public static CustomSnackbar make(ViewGroup parent, @Duration int duration) {
        // inflate custom layout
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View content = inflater.inflate(R.layout.snackbar_design, parent, false);

        // create snackbar with custom view
        ContentViewCallback callback= new ContentViewCallback(content);
        CustomSnackbar customSnackbar = new CustomSnackbar(parent, content, callback);
// Remove black background padding on left and right
        customSnackbar.getView().setPadding(0, 0, 0, 0);


        // set snackbar duration
        customSnackbar.setDuration(duration);
        return customSnackbar;
    }

    // set text in custom layout
    public CustomSnackbar setText(CharSequence text) {
        TextView text_snack_prize = (TextView) getView().findViewById(R.id.text_snack_prize);
        text_snack_prize.setText(text);
        return this;
    }

    // set action in custom layout
    public CustomSnackbar setAction(CharSequence text, final View.OnClickListener listener) {
        Button actionView = (Button) getView().findViewById(R.id.text_snack_viewcart);
        actionView.setText(text);
        actionView.setVisibility(View.VISIBLE);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(view);
                // Now dismiss the Snackbar
              /*  Toast.makeText(getContext(), "ni dikhate", Toast.LENGTH_SHORT).show();*/
                dismiss();
            }
        });
        return this;
    }

}
