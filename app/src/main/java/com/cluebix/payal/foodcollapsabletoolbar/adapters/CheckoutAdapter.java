package com.cluebix.payal.foodcollapsabletoolbar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cluebix.payal.foodcollapsabletoolbar.R;
import com.cluebix.payal.foodcollapsabletoolbar.datas.CheckoutCartData;
import com.cluebix.payal.foodcollapsabletoolbar.datas.MyOrderData;

import java.util.ArrayList;

public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.ViewHolder>{

    private Context context;
    //    private List<ListItemQuote> listItems;
    private ArrayList<CheckoutCartData> listItems;


    public CheckoutAdapter(Context context, ArrayList<CheckoutCartData> listItems) {
        this.context = context;
        this.listItems = listItems;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkout_adapter,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckoutAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(final View itemView) {
            super(itemView);
        }
    }


}
