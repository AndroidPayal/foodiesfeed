package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddAddressActivity extends AppCompatActivity {

    EditText cityEditText, streetEditText, localityEditText, pincodeEditText,
            stateEditText, landmarkEditText, nameEditText, mobileEditText;
    TextView submitButton;
    ArrayList<String> stateName;
    ArrayList<String> stateId;


    String addressID="";
    int selectedStatePosition = 0;
    private boolean checkEdit = false;
    private static boolean isValid = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);


 /*       cityEditText = findViewById(R.id.addAddress_cityText);

        streetEditText = findViewById(R.id.addAddress_streetText);

        localityEditText = findViewById(R.id.addAddres_localityText);

        pincodeEditText = findViewById(R.id.addAddres_pinText);

        landmarkEditText = findViewById(R.id.addAddres_landmarkText);

        nameEditText = findViewById(R.id.addAddres_nameText);

        mobileEditText = findViewById(R.id.addAddres_mobileText);

        submitButton = findViewById(R.id.addAddress_submitButton);


*/
        //check for empty fields

    /*        submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    isValid = true;


                    if (TextUtils.isEmpty(streetEditText.getText()) )
                    {
                        streetEditText.requestFocus();
                        streetEditText.setError("please provide input");

                        isValid = false;
                    }

                    if (TextUtils.isEmpty(localityEditText.getText()) )
                    {
                        localityEditText.requestFocus();
                        localityEditText.setError("please provide input");
                        isValid = false;
                    }

                    if (TextUtils.isEmpty(pincodeEditText.getText()) )
                    {
                        pincodeEditText.requestFocus();
                        pincodeEditText.setError("please provide input");
                        isValid = false;
                    } else if(pincodeEditText.getText().toString().length() < 6 ){
                        pincodeEditText.requestFocus();
                        pincodeEditText.setError("invalid pincode");
                        isValid = false;
                    }


                    if (TextUtils.isEmpty(landmarkEditText.getText()) )
                    {
                        landmarkEditText.requestFocus();
                        landmarkEditText.setError("please provide input");
                        isValid = false;
                    }

                    if (TextUtils.isEmpty(nameEditText.getText()) )
                    {
                        nameEditText.requestFocus();
                        nameEditText.setError("please provide input");
                        isValid = false;
                    }

                    if (TextUtils.isEmpty(mobileEditText.getText()) )
                    {
                        mobileEditText.requestFocus();
                        mobileEditText.setError("please provide input");
                        isValid = false;
                    } else if(mobileEditText.getText().toString().length() < 10 ){
                        mobileEditText.requestFocus();
                        mobileEditText.setError("invalid phone number");
                        isValid = false;
                    }


                    //checks if field was edited

                    if (checkEdit && isValid)
                    {
                        //updateAddress();
                    }
                    else if(isValid)
                    {
                        createAddress();
                    }

                }
            });

*/
    }





    // create address creates address
/*

    void createAddress()
    {

            submitButton.setEnabled(false);
            String url = "";//GlobalData.addUserAddress;

            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("createres", "" + response);

                    try {
                        JSONObject parentObject = new JSONObject(response);

                        String success = parentObject.getString("success");

                        if (success.equals("true"))
                        {
                            Toast.makeText(getApplicationContext(),"success", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(getApplicationContext(),ManageAddressActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finishAfterTransition();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"failed", Toast.LENGTH_SHORT).show();
                            submitButton.setEnabled(true);
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(),"error", Toast.LENGTH_SHORT).show();
                        submitButton.setEnabled(true);
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(getApplicationContext(),"error "+error, Toast.LENGTH_SHORT).show();
                    submitButton.setEnabled(true);

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    //        user, street,  locality,  landmark, state, city, pin, mobile, name

                    String user, street, locality, landmark, state, city, pin, mobile, name;


                    street = streetEditText.getText().toString();

                    locality = localityEditText.getText().toString();

                    landmark = landmarkEditText.getText().toString();

                    state = "" + selectedStatePosition;

                    city = cityEditText.getText().toString();

                    pin = pincodeEditText.getText().toString();

                    mobile = mobileEditText.getText().toString();

                    name = nameEditText.getText().toString();

              //      params.put("user", session==null?"":session.getUserDetails().get("userId"));


                    //        user, street,  locality,  landmark, state, city, pin, mobile, name

                    params.put("street", street);

                    params.put("locality", locality);

                    params.put("landmark", landmark);

                    params.put("state", state);

                    params.put("city", city);

                    params.put("pin", pin);

                    params.put("mobile", mobile);

                    params.put("name", name);


                    return params;
                }
            };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        }
*/

    }


