package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


public class CartActivity extends AppCompatActivity {


    LinearLayout linear_checkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        linear_checkout=findViewById(R.id.linear_checkout);
        linear_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CartActivity.this,CheckoutActivity.class);
                startActivity(i);
            }
        });

    }

    public void imageClick(View view){

        startActivity(new Intent(getApplicationContext(),OrderDetail.class));
    }




}
