package com.cluebix.payal.foodcollapsabletoolbar.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cluebix.payal.foodcollapsabletoolbar.R;
import com.cluebix.payal.foodcollapsabletoolbar.models.HorizontalCategoryMenu;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HorizontalCategoryMenuAdapter extends RecyclerView.Adapter<HorizontalCategoryMenuAdapter.ViewHolder>{


        private Context context;
        //    private List<ListItemQuote> listItems;
        private ArrayList<HorizontalCategoryMenu> listItems;
        private ArrayList<TextView> textViewObjects;
        private ClickListener clickListener;

        public HorizontalCategoryMenuAdapter(Context context, ArrayList listItems) {
            this.context = context;
            this.listItems = listItems;

            textViewObjects = new ArrayList<>();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.horizontal_category_menu_layout,parent,false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(HorizontalCategoryMenuAdapter.ViewHolder holder, int position) {

//        ListItemQuote item = listItems.get(position);

            HorizontalCategoryMenu item = listItems.get(position);



            holder.textView.setText(item.getCategoryName());

            if (item.isHighlighted()){
                holder.textView.setBackgroundResource(R.drawable.capsule_lined);
                holder.textView.setPadding(24,24,24,24);
                holder.textView.setTextColor(ContextCompat.getColor(context,R.color.green_accent));
            }

//        holder.name.setText(item.getName());
//        holder.description.setText(item.getDescription());
//        holder.rating.setText(item.getRating());
        }

        @Override
        public int getItemCount() {
            Log.d("listitemSize",""+listItems.size());
            return listItems.size();


        }

        public void setClickListener(ClickListener clickListener){
            this.clickListener = clickListener;
        }



        public class ViewHolder extends RecyclerView.ViewHolder {
            public LinearLayout layout;
            public TextView textView;

            public ViewHolder(final View itemView) {

                super(itemView);

                textView = itemView.findViewById(R.id.horizontal_menu_recycler_textView);


                textViewObjects.add(textView);

                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        int pos = getAdapterPosition();

                        for(int i=0; i <textViewObjects.size(); i++){

                            textViewObjects.get(i).setBackgroundResource(R.drawable.capsule_button);
                            textViewObjects.get(i).setTextColor(ContextCompat.getColor(context,R.color.white));
                            textViewObjects.get(i).setPadding(24,24,24,24);
                        }


                        if (clickListener!=null){
                            clickListener.textViewClicked(v,getAdapterPosition());
                        }


                    }
                });

                }


            }


    public interface ClickListener {

        void textViewClicked(View view, int pos);
    }


}



