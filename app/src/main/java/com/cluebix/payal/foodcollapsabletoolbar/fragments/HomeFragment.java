package com.cluebix.payal.foodcollapsabletoolbar.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cluebix.payal.foodcollapsabletoolbar.OrderDetail;
import com.cluebix.payal.foodcollapsabletoolbar.R;

public class HomeFragment extends Fragment {

    View v;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("Tag_frag", "onCreateView: frag2");

        if (container==null) {
            v = inflater.inflate(R.layout.fragment_one, container, true);
        }else{
            container.removeAllViews();
            v = inflater.inflate(R.layout.fragment_one, container, false);
        }
        return v;
    }



}
