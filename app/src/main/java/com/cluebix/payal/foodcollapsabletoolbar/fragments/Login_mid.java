package com.cluebix.payal.foodcollapsabletoolbar.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.cluebix.payal.foodcollapsabletoolbar.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login_mid extends Fragment  {
    View v;
    TextInputEditText login_email,login_pass;
    TextView login2_submit,login2_backScreen;
       Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (container==null) {
             v = inflater.inflate(R.layout.login_mid, container, true);
        }else{
             container.removeAllViews();
             v = inflater.inflate(R.layout.login_mid, container, false);
        }

   /*     login_email=(TextInputEditText)v.findViewById(R.id.login_email);
        login_pass=(TextInputEditText)v.findViewById(R.id.login_pass);
        login2_submit=(TextView)getActivity().findViewById(R.id.login2_submit);
*/
      /*  forgot_pw=getActivity().findViewById(R.id.login2_forgot_pw);
        forgot_pw.setVisibility(View.VISIBLE);
*/
       /* login2_submit.setOnClickListener(this);
        login2_backScreen.setOnClickListener(this);
        forgot_pw.setOnClickListener(this);*/
        return v;
    }

    /*



    @Override
    public void onClick(View v)
    {
        switch (v.getId()){
            case R.id.login2_submit:
                login_function();

                break;
            case R.id.login2_backScreen:
                //inflate_acc_fragment();
              //  sessionManager.logoutUser();
            //    getActivity().finish();
                getActivity().finish();
                Log.d("Tags_", "onClick: login back");
                break;
            case R.id.login2_forgot_pw:
                dialog=new Dialog(getContext());
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_forgot_pw);
                dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

                final TextView back=dialog.findViewById(R.id.dialog_back);
                TextView dialog_Reset=dialog.findViewById(R.id.dialog_reset);
                final EditText edit_dialog_email=dialog.findViewById(R.id.edit_dialog_email);

                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog_Reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String email=edit_dialog_email.getText().toString().trim();
                        if (!email.isEmpty() && email.matches(".+\\@.+\\..+")){


                        //hit api of forgot password
                        StringRequest stringRequest=new StringRequest(Request.Method.POST, GlobalData.Forgot_password, new Response.Listener<String>(){
                            @Override
                            public void onResponse(final String response) {
                                Log.d("login_response",response);
                                try {
                                    JSONObject res=new JSONObject(response);
                                    if (res.has("success")){
                                        final AlertDialog.Builder alertDialog=new AlertDialog.Builder(getContext());
                                        alertDialog.setTitle("Reset Password")
                                                .setMessage("You Are Done. \nCheck your mail for password reset link.")
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        //do nothing//only informative alert dialog
                                                    }
                                                }).show();

                                        dialog.cancel();
                                    }
                                    else{
                                        Toast.makeText(getContext(), "Please try again!", Toast.LENGTH_SHORT).show();
                                    }

                                   // Toast.makeText(getContext(), ""+response, Toast.LENGTH_SHORT).show();
                                }catch (Exception ex){
                                    Log.e("exception_login",ex.toString());}
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //dialog.dismiss();
                                Log.d("Login_error_response",error.toString());
                                Toast.makeText(getActivity(), "Server Connection Failed!", Toast.LENGTH_SHORT).show();
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String , String> parameters= new HashMap<String, String>();
                                parameters.put("email",email);
                                return parameters;
                            }
                        };


                        RquestHandler.getInstance(getActivity()).addToRequestQueue(stringRequest);

                        }else{edit_dialog_email.setError("Invalid!");}


                     *//*   final AlertDialog.Builder alertDialog=new AlertDialog.Builder(getContext());
                        alertDialog.setTitle("Reset Password")
                        .setMessage("You Are Done! \nCheck your mail for password reset link.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        }).show();


                        dialog.cancel();*//*
                    }
                });

                dialog.show();
                break;
        }
    }

    private void login_function()
    {
        final String mail=login_email.getText().toString().trim();
        final String pass=login_pass.getText().toString().trim();
        if (sessionManager.isLoggedIn()){
            Log.d("sessionscreen","login_ session created already");
        }else{ Log.d("sessionscreen","no previous login session"); }
        StringRequest stringRequest=new StringRequest(Request.Method.POST, login_url, new Response.Listener<String>(){
            @Override
            public void onResponse(final String response) {
                Log.d("login_response",response);
                try {
                    JSONObject obj=new JSONObject(response);
                    String success=obj.getString("success");

                    if (success.equalsIgnoreCase("true")){
                        inflate_acc_fragment();
                        JSONObject userData=obj.getJSONObject("user_data");
                        String userId=userData.getString("id");
                        String fisrt_name=userData.getString("fisrt_name");
                        String last_name=userData.getString("last_name");
                        String email=userData.getString("email");

                        sessionManager.createLoginSession(userId, fisrt_name, last_name, email,"0");
                       *//* if (sessionManager.isLoggedIn()){
                            Log.d("sessionscreen","login_ session created successfully");
                        }
*//*
                    }
                    Toast.makeText(getActivity(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Log.d("login_error_res","JSONException -"+e);
                }catch (Exception ex){
                    Log.e("exception_login",ex.toString());}
*//*{
    "error": "true",
    "success": "false",
    "message": "Something went wrong"
}*//*
*//*{
    "success": "true",
    "message": "User Authenticated successfully",
    "user_data": {
        "id": "1",
        "fisrt_name": "Ashwini",
        "last_name": "Mangar",
        "email": "ash@gmail.com",
        "user_type": "super",
        "is_email_verified": "0",
        "is_mobile_verified": "0"
    },
    "data_json": null
}*//*
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //dialog.dismiss();
                Log.d("Login_error_response",error.toString());
                Toast.makeText(getActivity(), "Server Connection Failed!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> parameters= new HashMap<String, String>();
                parameters.put("email",mail);
                parameters.put("password",pass);
                return parameters;
            }
        };


        RquestHandler.getInstance(getActivity()).addToRequestQueue(stringRequest);


    }

    private void inflate_acc_fragment()
    {

  *//*      FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        *//**//*switch (fragmentNo){
            case 0:*//**//* Fragment fragment = new Account_with_login();
     *//**//*           break;
            case 1:fragment = new Registration_mid();
                break;

        }*//**//*
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.login2_fragmentContainer, fragment).commit();*//*

        Intent i=new Intent(getActivity(), MainScreenActivity.class);
        i.putExtra("AccountTransition",true);
        startActivity(i);
        //getActivity().finish();



    }



    */

}
