package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.cluebix.payal.foodcollapsabletoolbar.adapters.CheckoutAdapter;
import com.cluebix.payal.foodcollapsabletoolbar.datas.CheckoutCartData;

import java.util.ArrayList;

public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView checkoutRecyclerView;
    ArrayList<CheckoutCartData> cartListItem_checkout;
    CheckoutAdapter checkoutAdapter;
    RelativeLayout checkout_createAddress_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        checkoutRecyclerView=findViewById(R.id.checkoutRecyclerView);
        checkout_createAddress_layout=findViewById(R.id.checkout_createAddress_layout);

        checkout_createAddress_layout.setOnClickListener(this);
        loadCartData();




    }




    void loadCartData()
    {

        cartListItem_checkout = new ArrayList<>();
        cartListItem_checkout.add(new CheckoutCartData("0"));
        cartListItem_checkout.add(new CheckoutCartData("0"));
        cartListItem_checkout.add(new CheckoutCartData("0"));
        cartListItem_checkout.add(new CheckoutCartData("0"));


        LinearLayoutManager layoutManager = new LinearLayoutManager(CheckoutActivity.this);

        checkoutRecyclerView.setHasFixedSize(true);

        checkoutAdapter = new CheckoutAdapter(CheckoutActivity.this,cartListItem_checkout);

        checkoutRecyclerView.setLayoutManager(layoutManager);

        checkoutRecyclerView.setAdapter(checkoutAdapter);


/*

        StringRequest stringRequest=new StringRequest(Request.Method.POST,GlobalData.List_all_cart_items , new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                // Log.d(Tag1_,"server response="+response);
                JSONObject post_data;
                try {

                    JSONObject obj=new JSONObject(response);
                    String success=obj.getString("success");
                    String message=obj.getString("message");

                    JSONArray jsonArray=obj.getJSONArray("items");

                    //clear arraylist

                    cartListItem.clear();

                    for(int i=0;i<jsonArray.length();i++) {
                        post_data = jsonArray.getJSONObject(i);

                        *//*{"success":"true","message":"Cart fetched successfully","items":[{"id":"4","user_id":"1","product_variant_id":"1","product_name":"Product","qty":"3","price":"250.00","image":"7.jpg","product_type":"0","design_image":null,"design_text":null}]}
                         * *//*
                        String id = post_data.getString("id");
                        String user_id = post_data.getString("user_id");
                        String product_variant_id= post_data.getString("product_variant_id");

                        String product_name=post_data.getString("product_name");
                        String qty = post_data.getString("qty");
                        String price = post_data.getString("price");
                        String image = post_data.getString("image");
                        String product_type=post_data.getString("product_type");
                        String design_image=post_data.getString("design_image");
                        String design_text=post_data.getString("design_text");


                        cartListItem.add(new cart_fetched_data(id,user_id,product_variant_id,product_name,qty,price,image,product_type,design_image,design_text));

                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(CheckoutActivity.this);

                    recyclerView.setHasFixedSize(true);

                    checkoutProductListAdapter = new CheckoutProductListAdapter(CheckoutActivity.this,cartListItem);

                    recyclerView.setLayoutManager(layoutManager);

                    recyclerView.setAdapter(checkoutProductListAdapter);




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CheckoutActivity.this, "Server Connection Failed!", Toast.LENGTH_SHORT).show();
            }
        })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("user", session==null?"":session.getUserDetails().get("userId"));//dummy id for now
                return parameters;
            }
        };

        RquestHandler.getInstance(CheckoutActivity.this).addToRequestQueue(stringRequest);*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
           case  R.id.checkout_createAddress_layout:
               Intent intent1 = new Intent(getApplicationContext(),ManageAddressActivity.class);
               startActivity(intent1);
            break;
        }
    }
}
