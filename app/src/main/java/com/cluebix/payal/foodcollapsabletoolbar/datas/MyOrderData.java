package com.cluebix.payal.foodcollapsabletoolbar.datas;

import java.util.ArrayList;

public class MyOrderData {
   private String orderIdDB        //this id is unique id of this order in database
            ,orderIdShow    //this id is id of order shown to user
            ,date,price,status;
   private ArrayList<String> foodName,foodQty;

    public MyOrderData(){}

    public MyOrderData(String orderIdDB,String orderIdShow,String date, String price, String status,
                        ArrayList<String> foodname,ArrayList<String> foodQty){
        this.orderIdDB=orderIdDB;
        this.orderIdShow=orderIdShow;
        this.date=date;
        this.price=price;
        this.status=status;
        this.foodName=foodname;
        this.foodQty=foodQty;
    }





    public String getOrderIdDB() {
        return orderIdDB;
    }

    public String getOrderIdShow() {
        return orderIdShow;
    }

    public String getDate() {
        return date;
    }

    public String getPrise() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<String> getFoodName() {
        return foodName;
    }

    public ArrayList<String> getFoodQty() {
        return foodQty;
    }








    public void setOrderIdDB(String orderIdDB) {
        this.orderIdDB = orderIdDB;
    }

    public void setOrderIdShow(String orderIdShow) {
        this.orderIdShow = orderIdShow;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setFoodName(ArrayList<String> foodName) {
        this.foodName = foodName;
    }

    public void setFoodQty(ArrayList<String> foodQty) {
        this.foodQty = foodQty;
    }
}
