package com.cluebix.payal.foodcollapsabletoolbar.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cluebix.payal.foodcollapsabletoolbar.R;
import com.cluebix.payal.foodcollapsabletoolbar.models.HorizontalCategoryMenu;
import com.cluebix.payal.foodcollapsabletoolbar.models.ProductData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{


    private Context context;
    //    private List<ListItemQuote> listItems;
    private ArrayList<ProductData> listItems;
    private ArrayList<Integer> imageArray;

    public ProductAdapter(Context context, ArrayList listItems, ArrayList<Integer> imageArray) {
        this.context = context;
        this.listItems = listItems;
        this.imageArray = imageArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_recycler_adapter,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {

//        ListItemQuote item = listItems.get(position);


        Picasso.with(context).load(imageArray.get(position)).
                placeholder(R.drawable.attention).into(holder.itemImage);

        ProductData item = listItems.get(position);


        holder.itemName.setText(item.getItemName());
        holder.itemPrice.setText(item.getItemPrice());

//        holder.name.setText(item.getName());
//        holder.description.setText(item.getDescription());
//        holder.rating.setText(item.getRating());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public ImageView itemImage;
        public TextView itemName;
        public TextView itemPrice;

        public ViewHolder(final View itemView) {

            super(itemView);

            layout = itemView.findViewById(R.id.product_recycler_layout);
            itemImage = itemView.findViewById(R.id.product_recycler_itemImage);
            itemName = itemView.findViewById(R.id.product_recycler_itemNameText);
            itemPrice = itemView.findViewById(R.id.product_recycler_itemPriceText);

        }


    }}



