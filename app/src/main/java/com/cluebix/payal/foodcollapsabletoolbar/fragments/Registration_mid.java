package com.cluebix.payal.foodcollapsabletoolbar.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cluebix.payal.foodcollapsabletoolbar.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Registration_mid extends Fragment implements View.OnClickListener {

    String Tag2_="server_error_RegistrationFragment", Tag1_="server_correct_RegistrationFragment";
    public static String selectedGender="male";//female

    View v;
    Toolbar toolbar;
    TextView register_done,  gender_selected_male,gender_selected_female;
    TextInputEditText register_edit_mobile,register_edit_pass2,register_edit_pass1
            ,register_edit_email,register_edit_lastName;
    EditText register_edit_firstName;
    String gender,mobile,pass1,pass2,email,lastname,firstname;
    TextView forgot_pw;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       if (container==null)
        v =inflater.inflate(R.layout.register_mid, container, true);
       else {
           container.removeAllViews();
           v =inflater.inflate(R.layout.register_mid, container, true);
       }

       Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        register_done=(TextView)getActivity().findViewById(R.id.login2_submit);
        register_edit_mobile=(TextInputEditText)v.findViewById(R.id.register_edit_mobile);
 /*       register_edit_pass2=(TextInputEditText)v.findViewById(R.id.register_edit_pass2);
        register_edit_pass1=(TextInputEditText)v.findViewById(R.id.register_edit_pass1);
        register_edit_email=(TextInputEditText)v.findViewById(R.id.register_edit_email);
        register_edit_lastName=(TextInputEditText)v.findViewById(R.id.register_edit_lastName);
        register_edit_firstName=(EditText) v.findViewById(R.id.register_edit_firstName);
        gender_selected_male=(TextView)v.findViewById(R.id.gender_selected_male);
        gender_selected_female=(TextView)v.findViewById(R.id.gender_selected_female);
*//*
        forgot_pw=getActivity().findViewById(R.id.login2_forgot_pw);
        forgot_pw.setVisibility(View.GONE);
*/

  /*      selectedGender="male";
        gender_selected_male.setBackgroundColor(getResources().getColor(R.color.lighter_grey));

        gender_selected_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedGender="male";
                gender_selected_male.setBackgroundColor(getResources().getColor(R.color.lighter_grey));
                gender_selected_female.setBackgroundColor(getResources().getColor(R.color.white));
            }
        });
        gender_selected_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedGender="female";
                gender_selected_male.setBackgroundColor(getResources().getColor(R.color.white));
                gender_selected_female.setBackgroundColor(getResources().getColor(R.color.lighter_grey));
            }
        });

        register_done.setOnClickListener(this);
*/        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
       /* if (v.getId()==R.id.login2_submit){
            gender = selectedGender;//register_edit_gender.getText().toString().trim();
            mobile = register_edit_mobile.getText().toString().trim();
            pass1 = register_edit_pass1.getText().toString().trim();
            pass2 = register_edit_pass2.getText().toString().trim();
            email = register_edit_email.getText().toString().trim();
            lastname = register_edit_lastName.getText().toString().trim();
            firstname = register_edit_firstName.getText().toString().trim();

            if (pass1.equals(pass2)){//password is correct
               if (!firstname.isEmpty()){
                   if(!email.isEmpty() && email.matches(".+\\@.+\\..+") ){
                       if(!pass1.isEmpty()){
                           if( mobile.length()==10){
                              // if(!gender.isEmpty()){
                              // Log.d("Tag_gender", "onClick: gender="+gender);
                                            send_data_to_server();
                              *//* }else{
                                   register_edit_gender.setError("Can't be Empty!");
                               }*//*
                           }else{
                               register_edit_mobile.setError("Must be 10 digit!");
                           }
                       }else{
                           register_edit_pass1.setError("Can't be Empty!");
                       }
                   }else{
                       register_edit_email.setError("Invalid email!");
                   }
               }else{
                    register_edit_firstName.setError("Can't be Empty!");
               }
            }else{
                register_edit_pass1.setError("Not same");
                register_edit_pass2.setError("Not same");
                Toast.makeText(getActivity(), "Unmatched Password", Toast.LENGTH_SHORT).show();
            }

        }else{
            if (v.getId()==R.id.login2_backScreen){
                Log.d("Tag_register", "onClick: backscreen register clicked");
                try{
                    getActivity().finish();
                }catch (Exception e){
                    Log.d("Tag_register", "onClick: exception:"+e.toString());
                }

            }
        }*/
    }

    /*

    private void send_data_to_server() {
        final ProgressDialog dialog= ProgressDialog.show(getContext(),"","Varifing data...");
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url_register_user, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                Log.d(Tag1_,"response="+response);
                try{
                    JSONObject obj=new JSONObject(response);
                    String success=obj.getString("success");
                    String message=obj.getString("message");
                    String userId=obj.getString("user_id");
                    //store user id in session

                    if(success.equalsIgnoreCase("true"))
                        sessionManager.createLoginSession(userId , firstname , lastname , email,"0");

                    dialog.dismiss();
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                   *//* if (sessionManager.isLoggedIn()){
                        Log.d("sessionscreen","registraton_ session created successfully");
                    }*//*
                }catch (Exception e){
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Wrong response from server", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
 *//*{
    "error": "true",
    "success": "false",
    "message": "Something went wrong",//we are toasting only msg bcz validation we performe manually and bcz email already registerd news willl appear in message field
    "validation_errors": "<p>The Mobile Number field must be at least 10 characters in length.</p>\n",
    "user_id": null
}*//**//*
{
"success":"true","message":"user registered successfully","user_id":9}
*//*
           }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(Tag2_,"volley error response="+error+"");
                dialog.dismiss();
                Toast.makeText(getActivity(), "Server Connection Failed!", Toast.LENGTH_SHORT).show();
            }
        })
        {   @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("email", email);//dummy id for now
                    parameters.put("password", pass1);
                    parameters.put("f_name", firstname);
                    parameters.put("l_name", lastname);
                    parameters.put("gender", gender);
                    parameters.put("mobile_no", mobile);
            Log.d("Tag_register", "send_data_to_server: "+email+"\n"+pass1+"\n"+firstname+"\n"+gender+"\n"+mobile);

            return parameters;
            }
        };
        RquestHandler.getInstance(getActivity()).addToRequestQueue(stringRequest);

    }

    */

}
