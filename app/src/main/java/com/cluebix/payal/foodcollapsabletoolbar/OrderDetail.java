package com.cluebix.payal.foodcollapsabletoolbar;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.baoyachi.stepview.HorizontalStepView;
import com.baoyachi.stepview.VerticalStepView;
import com.baoyachi.stepview.bean.StepBean;

import java.util.ArrayList;
import java.util.List;


public class OrderDetail extends AppCompatActivity {

    VerticalStepView verticalStepView;
    List<String> sources = new ArrayList<>();
    HorizontalStepView horizontalStepView;

    int i=0;
    int j=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        verticalStepView = findViewById(R.id.verticalStepview);

      /*  horizontalStepView = findViewById(R.id.horizontalalStepview);*/





        sources.add("Oreder accepted");
        sources.add("Food prepared");
        sources.add("Out for delivery");
        sources.add("Delivered");


        verticalStepView.setStepsViewIndicatorComplectingPosition(0)
                .reverseDraw(false)
                .setStepViewTexts(sources)
                .setLinePaddingProportion(0.85f)
                .setStepsViewIndicatorCompletedLineColor(Color.parseColor("#169C09"))
                .setStepViewComplectedTextColor(Color.parseColor("#000000"))
                .setStepViewUnComplectedTextColor(Color.parseColor("#000000"))
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(this,R.drawable.ic_check_circle_black_24dp))
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(this,R.drawable.ic_lens_focus_green_24dp));


       /*


       todo : working code for horizontal stepper

       List<StepBean> sources = new ArrayList<>();

        sources.add(new StepBean("Oreder accepted",1));
        sources.add(new StepBean("Food prepared",1));
        sources.add(new StepBean("Out for delivery",0));
        sources.add(new StepBean("Delivered",-1));


        horizontalStepView.setStepViewTexts(sources)
                .setTextSize(12)
                .setStepsViewIndicatorCompletedLineColor(Color.parseColor("#169C09"))
                .setStepViewComplectedTextColor(Color.parseColor("#000000"))
                .setStepViewUnComplectedTextColor(Color.parseColor("#000000"))
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(this,R.drawable.ic_check_circle_black_24dp))
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(this,R.drawable.ic_lens_focus_green_24dp));

*/





       new Thread(new Runnable() {
           @Override
           synchronized public void run() {

                for (i=0; i<sources.size(); i++){


                   try {
                       Thread.sleep(2000);

                       runOnUiThread(new Runnable() {
                           @Override
                           public void run() {

                               updateStepView(i);
                           }
                       });

                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }

               }


           }
       }).start();



    }


    synchronized public void updateStepView(int position){


        Log.d("position",""+position);

        verticalStepView.setStepsViewIndicatorComplectingPosition(j)
                .reverseDraw(false)
                .setStepViewTexts(sources)
                .setLinePaddingProportion(0.85f)
                .setStepsViewIndicatorCompletedLineColor(Color.parseColor("#169C09"))
                .setStepViewComplectedTextColor(Color.parseColor("#000000"))
                .setStepViewUnComplectedTextColor(Color.parseColor("#000000"))
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(this,R.drawable.ic_check_circle_black_24dp))
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(this,R.drawable.ic_lens_focus_green_24dp))
                .setStepsViewIndicatorDefaultIcon(ContextCompat.getDrawable(this,R.drawable.ic_adjust_black_24dp));

        ++j;

    }

}
