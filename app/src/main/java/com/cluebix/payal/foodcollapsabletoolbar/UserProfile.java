package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class UserProfile extends AppCompatActivity {

    DrawerLayout  drawerLayout;
    Toolbar toolbar_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_account);
        toolbar_order=findViewById(R.id.toolbar_account);
        initNavigationDrawer();


    }


    private void initNavigationDrawer() {

        final NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                int id = item.getItemId();

                switch (id) {
                    case R.id.home:
                        Intent i= new Intent(UserProfile.this,MainActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.homeSecond:
                        /*send to second fragment*/
                        i= new Intent(UserProfile.this,MainActivity.class);
                        i.putExtra("menuscreen",true);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.cartdrawer:
                        i= new Intent(UserProfile.this,CartActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.myorder:

                        drawerLayout.closeDrawers();
                        break;
                    case R.id.account:

                        i=new Intent(UserProfile.this, LoginRegister.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;

                }

                return true;
            }
        });


        View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView)header.findViewById(R.id.navigation_email);
        tv_email.setText("WELCOME Payal");


        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar_order,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }
            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }



}
