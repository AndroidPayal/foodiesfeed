package com.cluebix.payal.foodcollapsabletoolbar.models;

public class HorizontalCategoryMenu {

  String categoryName;
  boolean isHighlighted = false;

    public HorizontalCategoryMenu(String categoryName) {
        this.categoryName = categoryName;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isHighlighted() {
        return isHighlighted;
    }

    public void setHighlighted(boolean highlighted) {
        isHighlighted = highlighted;
    }
}
