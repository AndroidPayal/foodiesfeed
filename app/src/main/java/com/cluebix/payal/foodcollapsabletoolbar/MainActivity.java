package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cluebix.payal.foodcollapsabletoolbar.fragments.HomeFragment;
import com.cluebix.payal.foodcollapsabletoolbar.fragments.MenuFragment;
import com.cluebix.payal.foodcollapsabletoolbar.models.viewPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    ViewPager pager;
    viewPagerAdapter adapter;
    int []sale_image_array={R.drawable.foodlight11,R.drawable.foodlight3,R.drawable.foodlight2};
    private LinearLayout llPagerDots;
    private TextView[] dots;
    Toolbar toolbar;
    private TabLayout tabLayout;
    ViewPager tabs_viewpager;
    AppBarLayout appBarMainScreen;
    DrawerLayout  drawerLayout;
    Boolean bool_menuscreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.drawer_main);

       // bool_menuscreen = getIntent().getExtras().getBoolean("menuscreen");

        pager=(ViewPager)findViewById(R.id.sliderMain);
        llPagerDots = (LinearLayout) findViewById(R.id.pager_dots);
        toolbar=(Toolbar) findViewById(R.id.toolbar1);

        appBarMainScreen=(AppBarLayout)findViewById(R.id.appBarMainScreen);
        float heightDp = (getResources().getDisplayMetrics().heightPixels) / 2;
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams)appBarMainScreen.getLayoutParams();
        lp.height = (int)heightDp;


        //toolbar.setTitle(s);
        toolbar.inflateMenu(R.menu.activity_main_drawer);
        toolbar.setOnMenuItemClickListener(this);

        tabLayout=findViewById(R.id.tabs_main);
        tabs_viewpager=(ViewPager)findViewById(R.id.tabs_viewpager);
      //  if (getIntent().hasExtra("menuscreen")){
      //      tabs_viewpager.setCurrentItem(1);
       // }
        setupViewPager(tabs_viewpager);

        tabLayout.setupWithViewPager(tabs_viewpager);




        init();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 2000, 5000);

        initNavigationDrawer();

       if (getIntent().hasExtra("menuscreen")) {
             tabs_viewpager.setCurrentItem(1);
           //tabLayout.setupWithViewPager(tabs_viewpager);
          /* Log.d("Tag_tabpos", "onCreate: " + tabLayout.getSelectedTabPosition());
           if (tabLayout.getSelectedTabPosition() != 1) {
               tabLayout.getTabAt(1).select();
           }*/
       }

    }










    private void initNavigationDrawer() {

        final NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                int id = item.getItemId();

                switch (id) {
                    case R.id.home:
                        if (tabLayout.getSelectedTabPosition()!=0) {
                            tabLayout.getTabAt(0).select();
                        }
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.homeSecond:
                        /*send to second fragment*/
                        if (tabLayout.getSelectedTabPosition()!=1){
                            tabLayout.getTabAt(1).select();
                        }
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.cartdrawer:
                        Intent i= new Intent(MainActivity.this,CartActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.myorder:
                        i= new Intent(MainActivity.this,OrderHistory.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.account:

                   /*   i=new Intent(MainActivity.this, LoginRegister.class);
                      startActivity(i);*/
                        i=new Intent(MainActivity.this, UserProfile.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;

                }

                return true;
            }
        });


        View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView)header.findViewById(R.id.navigation_email);
        tv_email.setText("WELCOME Payal");


        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }
            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }










    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId()==R.id.search){
          /*  Intent i=new Intent(MainActivity.this,OrderHistory.class);
            startActivity(i);*/
        }
        return false;
    }











    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter2 adapter = new ViewPagerAdapter2(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new MenuFragment(), "Menu");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter2 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter2(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }











    private void init() {
        adapter=new viewPagerAdapter(getApplicationContext(),sale_image_array);
        pager.setAdapter(adapter);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    private void addBottomDots(int currentPage) {
        dots = new TextView[sale_image_array.length];
        llPagerDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getApplicationContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#000000"));
            llPagerDots.addView(dots[i]);
        }
        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }
    private class SliderTimer extends TimerTask {
        @Override
        public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (pager.getCurrentItem() < sale_image_array.length - 1) {
                            pager.setCurrentItem(pager.getCurrentItem() + 1);
                        } else {
                            pager.setCurrentItem(0);
                        }

                    }

                });
        }
    }




   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);

        MenuItem menuItem = menu.findItem(R.id.bottom_nav_cart);
         menuItem = menu.findItem(R.id.bottom_nav_home);

        return true;
    }*/



}
