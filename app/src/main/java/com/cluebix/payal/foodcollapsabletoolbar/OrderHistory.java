package com.cluebix.payal.foodcollapsabletoolbar;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cluebix.payal.foodcollapsabletoolbar.adapters.OrderAdapter;
import com.cluebix.payal.foodcollapsabletoolbar.datas.MyOrderData;
import com.cluebix.payal.foodcollapsabletoolbar.datas.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderHistory extends AppCompatActivity {

    RecyclerView recyclerView_order;
    OrderAdapter adapter;
    ArrayList<MyOrderData> ordered_food_list;
    DrawerLayout  drawerLayout;
    Toolbar toolbar_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_order);

        recyclerView_order=findViewById(R.id.recycler_orderlist);
        toolbar_order=findViewById(R.id.toolbar_order);

        recyclerView_order.setHasFixedSize(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView_order.setLayoutManager(layoutManager);
        recyclerView_order.setNestedScrollingEnabled(false);
        recyclerView_order.setFocusable(false);

        initNavigationDrawer();

        get_old_orders();
        adapter = new OrderAdapter(this,ordered_food_list);
        //adapter.setClickListener(this);
        recyclerView_order.setAdapter(adapter);


    }






    private void initNavigationDrawer() {

        final NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                int id = item.getItemId();

                switch (id) {
                    case R.id.home:
                        Intent i= new Intent(OrderHistory.this,MainActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.homeSecond:
                        /*send to second fragment*/
                         i= new Intent(OrderHistory.this,MainActivity.class);
                        i.putExtra("menuscreen",true);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.cartdrawer:
                         i= new Intent(OrderHistory.this,CartActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.myorder:

                        drawerLayout.closeDrawers();
                        break;
                    case R.id.account:
                      /*  Fragment newFragment = new CartFragment();
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                        transaction.replace(R.id.main_container, newFragment);
                        transaction.addToBackStack(null);

                        transaction.commit();*/
                        i=new Intent(OrderHistory.this, LoginRegister.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;

                }

                return true;
            }
        });


        View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView)header.findViewById(R.id.navigation_email);
        tv_email.setText("WELCOME Payal");


        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar_order,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }
            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }









    private void get_old_orders() {
        ordered_food_list=new ArrayList<>();
        ordered_food_list.clear();

        /*MyOrderData(String orderIdDB,String orderIdShow,String date, String prise, String status,
                        ArrayList<String> foodname,ArrayList<String> foodQty){*/
        ArrayList<String> foodname=new ArrayList<>();
        foodname.add("Paneer tadka");foodname.add("Idli sambhar");
        ArrayList<String> foodQty=new ArrayList<>();
        foodQty.add("2");foodQty.add("1");
        MyOrderData dataIn=new MyOrderData();
        dataIn.setOrderIdDB("0");
        dataIn.setOrderIdShow("0123456");
        dataIn.setDate("Sun, 29 Sept 2018");
        dataIn.setPrice(getResources().getString(R.string.rs)+" 500.00");
        dataIn.setStatus("Delivered");


        ordered_food_list.add(dataIn);ordered_food_list.add(dataIn);


        /*fetch data from network*/
               /* StringRequest stringRequest=new StringRequest(Request.Method.POST, Urls.orderHistory, new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderHistory","server response="+response);
                        JSONObject post_data;
                       *//* try {
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*//*
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(Tag2_,"volley error response="+error+"");
                        Toast.makeText(OrderHistory.this, "Server Connection Failed!", Toast.LENGTH_SHORT).show();
                       *//* progressBar.setVisibility(View.GONE);
                        network_failed.setVisibility(View.VISIBLE);*//*
                    }
                })
                {@Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("user", UserId);
                    return parameters;
                }
                };

                RquestHandler.getInstance(OrderHistory.this).addToRequestQueue(stringRequest);
 */

        }




    }




