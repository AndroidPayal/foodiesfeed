package com.cluebix.payal.foodcollapsabletoolbar.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cluebix.payal.foodcollapsabletoolbar.CartActivity;
import com.cluebix.payal.foodcollapsabletoolbar.adapters.HorizontalCategoryMenuAdapter;
import com.cluebix.payal.foodcollapsabletoolbar.adapters.ProductAdapter;
import com.cluebix.payal.foodcollapsabletoolbar.models.CustomSnackbar;
import com.cluebix.payal.foodcollapsabletoolbar.R;
import com.cluebix.payal.foodcollapsabletoolbar.models.HorizontalCategoryMenu;
import com.cluebix.payal.foodcollapsabletoolbar.models.ProductData;

import java.util.ArrayList;

public class MenuFragment extends Fragment implements View.OnClickListener, HorizontalCategoryMenuAdapter.ClickListener {
    View v;
    TextView textView_fragment;
    RecyclerView recyclerView,foodRecyclerView;
    HorizontalCategoryMenuAdapter adapter;
    Context context;
    HorizontalCategoryMenuAdapter.ClickListener clickListener;

    TextView textView2;
    CoordinatorLayout coordinatorLayout;
    RecyclerView.LayoutManager layoutManager, foodLayoutManager;
    ArrayList<HorizontalCategoryMenu> list;
    ArrayList<ProductData> southIndianList, desertList, chineseList;
    ArrayList<Integer> southIndianImages, desertImages, chineseImages;

    public MenuFragment(){}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("Tag_frag", "onCreateView: frag1");





        if (container==null) {
            v = inflater.inflate(R.layout.fragment_zero, container, true);
        }else{
            //container.removeAllViews();
            v = inflater.inflate(R.layout.fragment_zero, container, false);
        }
        


        context = getActivity();



        /*

        chinese

        south indian



        */




        textView_fragment=v.findViewById(R.id.textView_fragment);

        /*coordinatorLayout=getActivity().findViewById(R.id.coordinator1);*/

        recyclerView = v.findViewById(R.id.horizontal_menu_recyclerView);
        foodRecyclerView = v.findViewById(R.id.food_recyclerView);


        /*textView_fragment.setOnClickListener(this);*/


        list = new ArrayList<>();



        list.add(new HorizontalCategoryMenu("chinese"));
        list.add(new HorizontalCategoryMenu("south indian"));
        list.add(new HorizontalCategoryMenu("street food"));
        list.add(new HorizontalCategoryMenu("desert"));
        list.add(new HorizontalCategoryMenu("chineseasas"));
        list.add(new HorizontalCategoryMenu("south indianasa"));
        list.add(new HorizontalCategoryMenu("street foodaddw"));
        list.add(new HorizontalCategoryMenu("deserzzxcst"));

        chineseList = new ArrayList<>();
        southIndianList = new ArrayList<>();
        desertList = new ArrayList<>();

        southIndianImages = new ArrayList<>();
        desertImages = new ArrayList<>();
        chineseImages = new ArrayList<>();


        chineseList.add(new ProductData("Noodels","\u20B9 80"));
        chineseList.add(new ProductData("Munchurian","\u20B9 60"));
        chineseList.add(new ProductData("Egg Roll","\u20B9 40"));
        chineseList.add(new ProductData("Hakka Noodels","\u20B9 90"));
        chineseList.add(new ProductData("noodels","\u20B9 80"));
        chineseList.add(new ProductData("noodels","\u20B9 80"));

        chineseImages.add(R.drawable.food_chinese_noodles);
        chineseImages.add(R.drawable.food_chinese_manchurian);
        chineseImages.add(R.drawable.food_chinese_eggroll);
        chineseImages.add(R.drawable.food_chinese_hakkanoodles);
        chineseImages.add(R.drawable.food_chinese_noodles);
        chineseImages.add(R.drawable.food_chinese_noodles);




        southIndianList.add(new ProductData("Edli","\u20B9 40"));
        southIndianList.add(new ProductData("Plain Dosa","\u20B9 60"));
        southIndianList.add(new ProductData("Masala Dosa","\u20B9 40"));
        southIndianList.add(new ProductData("Uttapam","\u20B9 90"));
        southIndianList.add(new ProductData("Sambhar wada","\u20B9 80"));
        southIndianList.add(new ProductData("noodels","\u20B9 80"));

        southIndianImages.add(R.drawable.food_southindian_edli);
        southIndianImages.add(R.drawable.food_southindian_plaindosa);
        southIndianImages.add(R.drawable.food_southindian_masaladosa);
        southIndianImages.add(R.drawable.food_southindian_uttapam);
        southIndianImages.add(R.drawable.food_southindian_edli);
        southIndianImages.add(R.drawable.food_southindian_plaindosa);





        desertList.add(new ProductData("Kheer","\u20B9 40"));
        desertList.add(new ProductData("Ras malai","\u20B9 60"));
        desertList.add(new ProductData("Gulab jamun","\u20B9 40"));
        desertList.add(new ProductData("Rasgulla","\u20B9 90"));

        desertImages.add(R.drawable.food_desert_kheer);
        desertImages.add(R.drawable.food_desert_rasmalai);
        desertImages.add(R.drawable.food_desert_gulabjamun);
        desertImages.add(R.drawable.food_desert_rasgulla);






     /* list.add(new HorizontalCategoryMenu("chinese"));
        list.add(new HorizontalCategoryMenu("south indian"));
        list.add(new HorizontalCategoryMenu("street food"));
        list.add(new HorizontalCategoryMenu("desert"));
        list.add(new HorizontalCategoryMenu("chinese"));
        list.add(new HorizontalCategoryMenu("south indian"));
        list.add(new HorizontalCategoryMenu("street food"));
        list.add(new HorizontalCategoryMenu("desert"));
*/
        adapter = new HorizontalCategoryMenuAdapter(getContext(), list);


        clickListener = this;

        adapter.setClickListener(clickListener);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }

    @Override
    public void onClick(final View v) {
      /*  Snackbar snackbar = Snackbar
                .make(v, "Rs 800", Snackbar.LENGTH_LONG)
                .setAction("View Cart", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(v, "Message is restored!", Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                    }
                });
        snackbar.show();*/



        /*todo: previously working code
        * ************************************************
        * */

      /*  CustomSnackbar customSnackbar = CustomSnackbar.make(coordinatorLayout, 3000);
        customSnackbar.setText("Rs 500");
        customSnackbar.setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: handle click here
                Toast.makeText(getContext(), "Done creation", Toast.LENGTH_SHORT).show();
            }
        });
        customSnackbar.show();*/

      /*************************************************************/



      startActivity(new Intent(getActivity().getApplicationContext(), CartActivity.class));


    }

    @Override
    public void textViewClicked(View view, int pos) {


      /*  TextView textView = (TextView) view;

        textView.setBackgroundResource(R.drawable.capsule_lined);
        textView.setPadding(24,24,24,24);
        textView.setTextColor(ContextCompat.getColor(context,R.color.green_accent));
*/


        for (int i=0; i<list.size(); i++){
            HorizontalCategoryMenu dummyData = list.get(i);

            dummyData.setHighlighted(false);
            adapter.notifyItemChanged(i);
        }

        HorizontalCategoryMenu itemData = list.get(pos);


        itemData.setHighlighted(true);


        list.set(pos,itemData);
        adapter.notifyItemChanged(pos);
        adapter.notifyDataSetChanged();

        // set food recycler view

        //cases 0,1,3

        ProductAdapter productAdapter;

        foodLayoutManager = new GridLayoutManager(context,2);
        foodRecyclerView.setLayoutManager(foodLayoutManager);

        switch (pos){


            case 0: productAdapter = new ProductAdapter(context,chineseList,chineseImages);
            foodRecyclerView.setAdapter(productAdapter);
            break;

            case 1: productAdapter = new ProductAdapter(context,southIndianList,southIndianImages);
            foodRecyclerView.setAdapter(productAdapter);
            break;

            case 3: productAdapter = new ProductAdapter(context,desertList,desertImages);
            foodRecyclerView.setAdapter(productAdapter);
            break;

        }


    }
}
